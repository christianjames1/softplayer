import {render, waitFor} from '@testing-library/react'
import {CharacterDetail} from './features/character/CharacterDetail'
import {Provider} from 'react-redux'
import {store} from './app/store'
import {createMemoryHistory} from 'history'
import {Router} from 'react-router-dom'
import axios from 'axios'
import App from './App'

import character from './features/character/tests/fixtures/character.json'

test('Testando o título da página com a renderizacação do componente de detalhe', async () => {
  const mock = jest.spyOn(axios, 'get')
  mock.mockImplementation(() => Promise.resolve({data: {data: {results: [character]}}}))
  const history = createMemoryHistory()
  history.push('/character/view/1009351')

  const {getByTestId} = render(
    <Provider store={store}>
      <Router history={history}>
        <App />
        <CharacterDetail />
      </Router>
    </Provider>
  )

  await waitFor(() => {
    expect(getByTestId('title-page')).toHaveTextContent(/^Softplayer - Hulk$/)
  })
})

test('Verifica de na página inicial tem o título da aplicação', () => {
  const {getByTestId} = render(
    <Provider store={store}>
      <App />
    </Provider>
  )

  expect(getByTestId('title-page')).toHaveTextContent(/^Softplayer$/)
})
