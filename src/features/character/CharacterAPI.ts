import axios from 'axios'
import {ICharacter} from './ICharacter'

export const fetchCharacters = (name: string): Promise<ICharacter[]> => {
  return new Promise<ICharacter[]>(resolve => {
    let url =
      'https://gateway.marvel.com:443/v1/public/characters?ts=1&limit=100&hash=93ed3373c694543237ef163ca778967d&apikey=2aee81b4d5399bba4b77fe8521aebaaf'

    if (name) url += '&nameStartsWith=' + name

    axios.get(url).then(res => {
      resolve(res.data.data.results)
    })
  })
}

export const fetchCharacterById = (id: string): Promise<ICharacter> => {
  return new Promise<ICharacter>((resolve, reject) =>
    axios
      .get(
        'https://gateway.marvel.com:443/v1/public/characters/' +
          id +
          '?ts=1&limit=100&hash=93ed3373c694543237ef163ca778967d&apikey=2aee81b4d5399bba4b77fe8521aebaaf'
      )
      .then(res => {
        resolve(res.data.data.results[0])
      })
      .catch(() => {
        reject(id)
      })
  )
}
