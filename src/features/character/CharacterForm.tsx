import React, {useLayoutEffect} from 'react'
import {Link} from 'react-router-dom'
import {useParams} from 'react-router-dom'

import {useAppSelector, useAppDispatch} from '../../app/hooks'

import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Backdrop from '@material-ui/core/Backdrop'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import SaveIcon from '@material-ui/icons/Save'
import CloseIcon from '@material-ui/icons/Close'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import {useHistory} from 'react-router-dom'
import AlertIcon from '@material-ui/icons/Warning'

import {
  loadCharacterByIdAsync,
  selectCharacter,
  selectLoading,
  saveCharacter,
  changeValue,
  changeValueThumb,
} from './CharacterSlice'

import useCharacterBaseStyle from './styles/CharacterBaseStyle'
import useStyles from './styles/CharacterFormStyle'

export function CharacterForm(): JSX.Element {
  const {id} = useParams<{id: string}>()

  const classesBase = useCharacterBaseStyle()
  const classes = useStyles()
  const character = useAppSelector(selectCharacter)
  const loading = useAppSelector(selectLoading)
  const thumbnailUrl = character?.thumbnail?.path && character.thumbnail.path + '.' + character.thumbnail.extension
  const history = useHistory()
  const dispatch = useAppDispatch()

  const maxLengthDescription = 350
  const maxLengthText = 255
  const charactersRemaining = maxLengthDescription - (character.description?.length ?? 0) + ' caracteres restantes'

  const saveForm = () => {
    dispatch(saveCharacter())
    history.push('/')
  }

  useLayoutEffect(() => {
    const fetchCharacterById = () => {
      return dispatch(loadCharacterByIdAsync(id))
    }
    fetchCharacterById()
  }, [dispatch, id])

  return (
    <div className={classes.root}>
      <Container>
        <Backdrop className={classes.backdrop} open={loading}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Link className={classes.link} to={'/'}>
              <Button variant="contained" color="primary" startIcon={<ArrowBackIcon />}>
                {' '}
                Voltar
              </Button>
            </Link>
          </Grid>
        </Grid>
        {!character.name && !loading && (
          <Paper elevation={3} className={classesBase.paperAlert}>
            <AlertIcon className={classesBase.paperAlertIcon} />
            <p className={classesBase.paperAlertMessage}>Nenhum personagem encontrado</p>
          </Paper>
        )}

        {character.id && (
          <form className={classes.form} noValidate autoComplete="off" onSubmit={saveForm} data-testid="form-character">
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <Paper square={true} className={classes.paper} elevation={3}>
                  <img width="100%" src={thumbnailUrl} alt={character.name} />
                </Paper>
              </Grid>
              <Grid item xs={12} md={6}>
                <Grid item xs={12}>
                  <TextField
                    id="standard-basic"
                    label="Id"
                    value={character.id}
                    variant="outlined"
                    type="number"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="standard-basic"
                    label="Nome"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{maxLength: maxLengthText, 'data-testid': 'nome'}}
                    value={character.name}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      dispatch(changeValue({name: 'name', value: e.target.value}))
                    }
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="campo-descricao"
                    data-testid="campo-descricao"
                    label="Descrição"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{maxLength: maxLengthDescription, 'data-testid': 'descricao'}}
                    multiline
                    maxRows={4}
                    variant="outlined"
                    value={character.description}
                    helperText={charactersRemaining}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      dispatch(changeValue({name: 'description', value: e.target.value}))
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="standard-basic"
                    label="Imagem"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{maxLength: maxLengthText, 'data-testid': 'imagem'}}
                    value={thumbnailUrl}
                    variant="outlined"
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => dispatch(changeValueThumb(e.target.value))}
                  />
                </Grid>

                <Grid className={classes.buttons} item xs={12}>
                  <Link className={classes.link} to={'/'}>
                    <Button variant="contained" color="secondary" startIcon={<CloseIcon />}>
                      Cancelar
                    </Button>
                  </Link>
                  <Button variant="contained" color="primary" type="submit" startIcon={<SaveIcon />} data-testid="botao-salvar">
                    Salvar
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </form>
        )}
      </Container>
    </div>
  )
}
