import React, {useLayoutEffect} from 'react'
import {Link} from 'react-router-dom'
import {useParams} from 'react-router-dom'

import {useAppSelector, useAppDispatch} from '../../app/hooks'

import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import AlertIcon from '@material-ui/icons/Warning'

import {loadCharacterByIdAsync, selectCharacter, selectLoading} from './CharacterSlice'

import useCharacterBaseStyle from './styles/CharacterBaseStyle'
import useStyles from './styles/CharacterFormStyle'

export function CharacterDetail(): JSX.Element {
  const {id} = useParams<{id: string}>()

  const classesBase = useCharacterBaseStyle()
  const classes = useStyles()
  const character = useAppSelector(selectCharacter)
  const loading = useAppSelector(selectLoading)
  const dispatch = useAppDispatch()

  const thumbnailUrl = character?.thumbnail?.path && character.thumbnail.path + '.' + character.thumbnail.extension

  useLayoutEffect(() => {
    const fetchCharacterById = () => {
      return dispatch(loadCharacterByIdAsync(id))
    }
    fetchCharacterById()
  }, [dispatch, id])

  return (
    <div className={classes.root}>
      <Container>
        <Backdrop className={classes.backdrop} open={loading}>
          <CircularProgress color="inherit" />
        </Backdrop>

        {!character.name && !loading && (
          <Paper elevation={3} className={classesBase.paperAlert}>
            <AlertIcon className={classesBase.paperAlertIcon} />
            <p className={classesBase.paperAlertMessage}>Nenhum personagem encontrado</p>
          </Paper>
        )}

        {character.name && (
          <form className={classes.form} noValidate autoComplete="off">
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Link className={classes.link} to={'/'}>
                  <Button variant="contained" color="primary" startIcon={<ArrowBackIcon />}>
                    {' '}
                    Voltar
                  </Button>
                </Link>
              </Grid>
              <Grid item xs={12} md={6}>
                <img data-testid={character.id + '-image'} width="100%" src={thumbnailUrl} alt={character.name} />
              </Grid>
              <Grid item xs={12} md={6}>
                <Grid item xs={12}>
                  <Typography variant="h4" gutterBottom data-testid="chraracter-name">
                    {character.name}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="body1" gutterBottom>
                    {character.description}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography gutterBottom variant="h6">
                    Series
                  </Typography>

                  {character?.series.items.map((serie, index) => (
                    <p key={index}>{serie.name}</p>
                  ))}
                </Grid>
              </Grid>
            </Grid>
          </form>
        )}
      </Container>
    </div>
  )
}
