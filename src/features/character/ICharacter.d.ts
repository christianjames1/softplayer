export interface IThumbnail {
  path: string
  extension: string
}

export interface ISeries {
  items: ISeriesItem[]
}

export interface ISeriesItem {
  name: string
}

export interface ICharacter {
  id: number
  name: string
  description: string
  thumbnail: IThumbnail
  series: ISeries
  sincronized: boolean
}

export interface ICharacterFromForm {
  id: string
  name: string
  description: string
  thumbnail: string
  sincronized: boolean
}

export interface ICharacterState {
  characters: ICharacter[]
  charactersLocal: ICharacter[]
  character: ICharacter
  status: 'idle' | 'loading' | 'failed'
  loading: boolean
  search: string
}

export interface IChangeValue {
  name: string
  value: string
}
