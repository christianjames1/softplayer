import App from '../../../App'
import {render, waitFor, screen, fireEvent} from '@testing-library/react'
import axios from 'axios'
import {createMemoryHistory} from 'history'
import {Router} from 'react-router-dom'
import {store} from '../../../app/store'
import {Provider} from 'react-redux'

import characters from './fixtures/characters.json'

import reducer, {setSearch} from '../CharacterSlice'

test('Testando a renderização de multiplos cards', async () => {
  const mock = jest.spyOn(axios, 'get')
  mock.mockImplementation(() => Promise.resolve({data: {data: {results: characters}}}))
  const history = createMemoryHistory()

  history.push('/')

  const {getByTestId, queryByText} = render(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>
  )

  await waitFor(() => {
    expect(getByTestId('title-page')).toHaveTextContent(/^Softplayer$/)
    expect(queryByText('Captain America 2')).toBeInTheDocument()
    expect(queryByText('Descrição capitão america')).toBeInTheDocument()
    expect(getByTestId('4214412-image')).toHaveStyle(
      `background-image: url(http://i.annihil.us/u/prod/marvel/i/mg/3/50/537ba56d31087.jpg)`
    )
    expect(queryByText('Hulk')).toBeInTheDocument()
    expect(queryByText('Doctor Strange')).not.toBeInTheDocument()
  })
})

test('Testando a renderização de multiplos cards com search', async () => {
  const mock = jest.spyOn(axios, 'get')
  mock.mockImplementation(() => Promise.resolve({data: {data: {results: characters}}}))
  const history = createMemoryHistory()

  history.push('/')

  render(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>
  )

  fireEvent.change(screen.getByTestId('search-character'), {target: {value: 'Hulk'}})

  // await store.dispatch(setSearch('Hulk'));

  await waitFor(() => {
    expect(store.getState().characters.search).toBe('Hulk')
  })
})
