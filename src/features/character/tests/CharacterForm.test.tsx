import {CharacterForm} from '../CharacterForm'
import {render, waitFor, screen, fireEvent} from '@testing-library/react'
import axios from 'axios'
import {createMemoryHistory} from 'history'
import {Router} from 'react-router-dom'
import {store} from '../../../app/store'
import {Provider} from 'react-redux'
import character from './fixtures/character.json'

test('Testando a renderização do card de edicao', async () => {
  const mock = jest.spyOn(axios, 'get')
  mock.mockImplementation(() => Promise.resolve({data: {data: {results: [character]}}}))
  const history = createMemoryHistory()
  history.push('/character/1009351')

  const {getByDisplayValue} = render(
    <Provider store={store}>
      <Router history={history}>
        <CharacterForm />
      </Router>
    </Provider>
  )

  await waitFor(() => {
    expect(getByDisplayValue('1009351')).toBeInTheDocument()
    expect(getByDisplayValue('Hulk')).toBeInTheDocument()
    expect(getByDisplayValue('Caught in a gamma bomb explosion while trying to save the life of a teenager')).toBeInTheDocument()
    expect(getByDisplayValue('http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0.jpg')).toBeInTheDocument()
  })
})

test('Testando a alteracao de campos no formulario e alteracao na store', async () => {
  const mock = jest.spyOn(axios, 'get')
  mock.mockImplementation(() => Promise.resolve({data: {data: {results: [character]}}}))
  const history = createMemoryHistory()
  history.push('/character/1009351')

  const {container} = render(
    <Provider store={store}>
      <Router history={history}>
        <CharacterForm />
      </Router>
    </Provider>
  )

  await waitFor(async () => {
    const campoDescricaoHelper = container.querySelector('#campo-descricao-helper-text')

    expect(campoDescricaoHelper).toHaveTextContent(/^274 caracteres restantes$/i)
    fireEvent.change(screen.getByTestId('nome'), {target: {value: 'Hulk alterado'}})
    fireEvent.change(screen.getByTestId('descricao'), {target: {value: 'Descricao Hulk'}})
    fireEvent.change(screen.getByTestId('imagem'), {
      target: {value: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg'},
    })

    expect(campoDescricaoHelper).toHaveTextContent(/^336 caracteres restantes$/i)
    expect(store.getState().characters.character.name).toBe('Hulk alterado')
    expect(store.getState().characters.character.description).toBe('Descricao Hulk')
    expect(store.getState().characters.character.thumbnail.path).toBe('http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16')
    expect(store.getState().characters.character.thumbnail.extension).toBe('jpg')

    fireEvent.submit(screen.getByTestId('form-character'))

    await waitFor(() => expect(store.getState().characters.charactersLocal[0].name).toBe('Hulk alterado'))
  })
})

test('Testando o reject', async () => {
  const mock = jest.spyOn(axios, 'get')
  mock.mockImplementation(() => Promise.reject({data: {data: {results: [character]}}}))
  const history = createMemoryHistory()
  history.push('/character/1009351')

  const {container} = render(
    <Provider store={store}>
      <Router history={history}>
        <CharacterForm />
      </Router>
    </Provider>
  )

  await waitFor(async () => {
    expect(store.getState().characters.character.name).not.toBe('Hulk')
  })
})
