import {CharacterDetail} from '../CharacterDetail'
import {render, waitFor} from '@testing-library/react'
import axios from 'axios'
import {createMemoryHistory} from 'history'
import {Router} from 'react-router-dom'
import {store} from '../../../app/store'
import {Provider} from 'react-redux'

import character from './fixtures/character.json'

test('Testando a renderização do card de detalhe', async () => {
  const mock = jest.spyOn(axios, 'get')
  mock.mockImplementation(() => Promise.resolve({data: {data: {results: [character]}}}))
  const history = createMemoryHistory()
  history.push('/character/view/1009351')

  const {getByTestId, queryByText} = render(
    <Provider store={store}>
      <Router history={history}>
        <CharacterDetail />
      </Router>
    </Provider>
  )

  await waitFor(() => {
    expect(getByTestId('chraracter-name')).toHaveTextContent('Hulk')
    expect(queryByText('5 Ronin (2010)')).toBeInTheDocument()
    expect(queryByText('Caught in a gamma bomb explosion while trying to save the life of a teenager')).toBeInTheDocument()
    expect(getByTestId('1009351-image')).toHaveAttribute('src', 'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0.jpg')
    expect(queryByText('Doctor Strange')).not.toBeInTheDocument()
  })
})
