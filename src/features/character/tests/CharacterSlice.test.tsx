import {ICharacter, ICharacterState} from '../ICharacter'

import reducer, {setLoading, setSearch, saveCharacter} from '../CharacterSlice'
import {store} from '../../../app/store'

test('Deve detornar o initial state', () => {
  expect(reducer(undefined, {})).toEqual({
    characters: [],
    charactersLocal: [],
    character: {} as ICharacter,
    status: 'idle',
    loading: false,
    search: '',
  })
})

test('Ao chamar a funcao setLoading o retorno deve ser com o loading:true', () => {
  expect(reducer(undefined, setLoading(true))).toEqual({
    characters: [],
    charactersLocal: [],
    character: {} as ICharacter,
    status: 'idle',
    loading: true,
    search: '',
  })
})

test('Ao chamar a funcao setSearch o retorno do propriedade search deve ser Teste unitário', () => {
  expect(reducer(undefined, setSearch('Teste unitário'))).toEqual({
    characters: [],
    charactersLocal: [],
    character: {} as ICharacter,
    status: 'idle',
    loading: false,
    search: 'Teste unitário',
  })
})

test('Ao chamar a funcao saveCharacter o retorno do character salvo na propriedade charactersLocal', () => {
  const character: ICharacter = {
    id: 4214412,
    name: 'Captain America 2',
    description: 'Descrição',
    thumbnail: {
      path: '',
      extension: '',
    },
    sincronized: false,
    series: {
      items: [],
    },
  }

  const previousState: ICharacterState = {
    characters: [],
    charactersLocal: [],
    character: character,
    status: 'idle',
    loading: false,
    search: '',
  }

  expect(reducer(previousState, saveCharacter())).toEqual({
    characters: [],
    charactersLocal: [character],
    character: {},
    status: 'idle',
    loading: false,
    search: '',
  })
})

test('Ao chamar a funcao saveCharacter com um characterLocal, deve atualizar ele ao em vez de adicionar novo no charactersLocal', () => {
  const character: ICharacter = {
    id: 4214412,
    name: 'Captain America 2',
    description: 'Descrição',
    thumbnail: {
      path: '',
      extension: '',
    },
    sincronized: false,
    series: {
      items: [],
    },
  }

  const characterAlterado: ICharacter = {
    id: 4214412,
    name: 'Captain America',
    description: 'Descrição',
    thumbnail: {
      path: '',
      extension: '',
    },
    sincronized: false,
    series: {
      items: [],
    },
  }

  const previousState: ICharacterState = {
    characters: [],
    charactersLocal: [character],
    character: characterAlterado,
    status: 'idle',
    loading: false,
    search: '',
  }

  expect(reducer(previousState, saveCharacter())).toEqual({
    characters: [],
    charactersLocal: [characterAlterado],
    character: {},
    status: 'idle',
    loading: false,
    search: '',
  })
})
