import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../../app/store'
import {ICharacter, ICharacterState, IChangeValue} from './ICharacter'
import {fetchCharacters, fetchCharacterById} from './CharacterAPI'

const initialState: ICharacterState = {
  characters: [],
  charactersLocal: [],
  character: {} as ICharacter,
  status: 'idle',
  loading: false,
  search: '',
}

export const loadCharactersAsync = createAsyncThunk('character/fetchCharacters', async (characterName: string) => {
  const data = await fetchCharacters(characterName)
  return data
})

export const loadCharacterByIdAsync = createAsyncThunk('character/fetchCharacterById', async (id: string, {rejectWithValue}) => {
  try {
    const data = await fetchCharacterById(id)
    return data
  } catch (err) {
    return rejectWithValue(+id)
  }
})

export const counterSlice = createSlice({
  name: 'character',
  initialState,
  reducers: {
    setSearch: (state, action: PayloadAction<string>) => {
      state.search = action.payload
    },
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload
    },
    changeValue: (state, action: PayloadAction<IChangeValue>) => {
      return {
        ...state,
        character: {
          ...state.character,
          [action.payload.name]: action.payload.value,
        },
      }
    },
    changeValueThumb: (state, action: PayloadAction<string>) => {
      const lastIndex = action.payload.lastIndexOf('.')

      const path = action.payload.substr(0, lastIndex)
      const extension = action.payload.substr(lastIndex + 1)

      return {
        ...state,
        character: {
          ...state.character,
          thumbnail: {
            path: path,
            extension: extension,
          },
        },
      }
    },
    saveCharacter: state => {
      const index = state.charactersLocal.findIndex(x => x.id === +state.character.id)
      const actualCharacter = {...state.character}
      actualCharacter.sincronized = false

      if (index > -1) {
        state.character = {} as ICharacter
        state.charactersLocal[index] = actualCharacter
      } else {
        return {
          ...state,
          character: {} as ICharacter,
          charactersLocal: [...state.charactersLocal.slice(0, index), actualCharacter, ...state.charactersLocal.slice(index + 1)],
        }
      }
    },
  },
  extraReducers: builder => {
    builder
      .addCase(loadCharactersAsync.pending, state => {
        state.status = 'loading'
        state.loading = true
        state.characters = []
        state.character = {} as ICharacter
      })
      .addCase(loadCharactersAsync.fulfilled, (state, action) => {
        state.status = 'idle'
        state.loading = false

        const chractersWithLocalChanges = action.payload.map(original => {
          return state.charactersLocal.find(local => local.id === original.id) ?? original
        })

        state.characters = chractersWithLocalChanges
      })
      .addCase(loadCharacterByIdAsync.pending, state => {
        state.status = 'loading'
        state.character = {} as ICharacter
        state.loading = true
      })
      .addCase(loadCharacterByIdAsync.fulfilled, (state, action) => {
        state.status = 'idle'
        state.loading = false
        state.character = state.charactersLocal.find(local => local.id === action.payload.id) ?? action.payload
      })
      .addCase(loadCharacterByIdAsync.rejected, (state, action) => {
        state.status = 'idle'
        state.loading = false
        state.character = state.charactersLocal.find(local => local.id === action.payload) ?? ({} as ICharacter)
      })
  },
})

export const {setLoading, setSearch, saveCharacter, changeValue, changeValueThumb} = counterSlice.actions

export const selectCharacters = (state: RootState): ICharacter[] => state.characters.characters
export const selectCharacter = (state: RootState): ICharacter => state.characters.character
export const selectLoading = (state: RootState): boolean => state.characters.loading
export const selectSearch = (state: RootState): string => state.characters.search

export default counterSlice.reducer
