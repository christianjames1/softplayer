import React, {useLayoutEffect} from 'react'
import {Link} from 'react-router-dom'

import {useAppSelector, useAppDispatch} from '../../app/hooks'

import IconButton from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/Edit'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'
import Paper from '@material-ui/core/Paper'
import AlertIcon from '@material-ui/icons/Warning'
import VisibilityIcon from '@material-ui/icons/Visibility'
import Tooltip from '@material-ui/core/Tooltip'

import useCharacterBaseStyle from './styles/CharacterBaseStyle'
import useStyles from './styles/CharacterStyle'

import {debounce} from 'ts-debounce'

import {loadCharactersAsync, selectCharacters, selectLoading, selectSearch, setSearch} from './CharacterSlice'

export function Character(): JSX.Element {
  const classesBase = useCharacterBaseStyle()
  const classes = useStyles()
  const characters = useAppSelector(selectCharacters)
  const loading = useAppSelector(selectLoading)
  const searchCharacter = useAppSelector(selectSearch)
  const dispatch = useAppDispatch()

  const setSearchCharacter = async (value: string) => dispatch(setSearch(value))

  const setSearchCharacterDebounce = debounce(setSearchCharacter, 500)

  useLayoutEffect(() => {
    const fetchCharacters = () => {
      return dispatch(loadCharactersAsync(searchCharacter))
    }
    fetchCharacters()
  }, [dispatch, searchCharacter])

  return (
    <Container>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper component="form" className={classes.root}>
            <InputBase
              className={classes.input}
              placeholder="Procurar Personagem"
              inputProps={{'aria-label': 'Procurar Personagem', 'data-testid': 'search-character'}}
              defaultValue={searchCharacter}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => setSearchCharacterDebounce(e.target.value)}
            />
            <IconButton className={classes.iconButton} aria-label="search">
              <SearchIcon />
            </IconButton>
          </Paper>
        </Grid>

        {characters.length === 0 && !loading && (
          <Paper elevation={3} className={classesBase.paperAlert}>
            <AlertIcon className={classesBase.paperAlertIcon} />
            <p className={classesBase.paperAlertMessage}>Nenhum personagem encontrado</p>
          </Paper>
        )}

        {characters.map((item, index) => (
          <Grid key={index} item xs={12} md={6} lg={3}>
            <Card className={classes.root} data-testid={item.id}>
              <CardMedia
                className={classes.media}
                image={item.thumbnail.path + '.' + item.thumbnail.extension}
                title={item.name}
                data-testid={item.id + '-image'}
              />
              <CardContent className={classes.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  {item.name}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p" className={classes.cardContentText}>
                  {item.description}
                </Typography>
              </CardContent>
              <CardActions className={classes.actions} disableSpacing>
                <Link to={'/character/' + item.id}>
                  <Tooltip title="Editar personagem">
                    <IconButton aria-label="Editar personagem">
                      <EditIcon />
                    </IconButton>
                  </Tooltip>
                </Link>
                <Link to={'/character/view/' + item.id}>
                  <Tooltip title="Visualizar personagem">
                    <IconButton aria-label="visualizar personagem">
                      <VisibilityIcon />
                    </IconButton>
                  </Tooltip>
                </Link>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  )
}
