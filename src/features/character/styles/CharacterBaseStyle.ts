import {createStyles, makeStyles} from '@material-ui/core/styles'

export default makeStyles(() =>
  createStyles({
    paperAlert: {
      color: '#fff',
      fontWeight: 500,
      backgroundColor: '#ff9800',
      display: 'flex',
      padding: '6px 16px',
      fontSize: '0.875rem',
      lineHeight: 1.43,
      borderRadius: '4px',
      letterSpacing: '0.01071em',
      width: '100%',
      marginTop: '10px',
    },
    paperAlertIcon: {
      display: 'flex',
      opacity: 0.9,
      padding: '7px 0',
      fontSize: '22px',
      marginRight: '12px',
    },
    paperAlertMessage: {
      padding: '8px 0',
      margin: 0,
    },
  })
)
