import {Theme, createStyles, makeStyles} from '@material-ui/core/styles'

export default makeStyles((theme: Theme) => {
  return createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
      marginTop: theme.spacing(2),
    },
    icon: {
      color: 'rgba(255, 255, 255, 0.54)',
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
    paper: {
      width: '100%',
      height: '100%',
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '100%',
      },
    },
    form: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '100%',
      },
    },
    buttons: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    link: {
      textDecoration: 'none',
    },
  })
})
