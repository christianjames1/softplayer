import {Theme, createStyles, makeStyles} from '@material-ui/core/styles'

export default makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
      marginTop: theme.spacing(2),
    },
    icon: {
      color: 'rgba(255, 255, 255, 0.54)',
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
    media: {
      width: '100%',
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    cardContent: {
      width: '100%',
      height: '100px ',
    },
    cardContentText: {
      display: '-webkit-box',
      maxWidth: '100%',
      '-webkitLineClamp': 3,
      '-webkitBoxOrient': 'vertical',
      overflow: 'hidden',
    },
    actions: {
      width: '100%',
      justifyContent: 'flex-end',
    },
    fab: {
      position: 'fixed',
      bottom: theme.spacing(2),
      right: theme.spacing(2),
    },
    paperAlert: {
      color: '#fff',
      fontWeight: 500,
      backgroundColor: '#ff9800',
      display: 'flex',
      padding: '6px 16px',
      fontSize: '0.875rem',
      lineHeight: 1.43,
      borderRadius: '4px',
      letterSpacing: '0.01071em',
      width: '100%',
    },
    paperAlertIcon: {
      display: 'flex',
      opacity: 0.9,
      padding: '7px 0',
      fontSize: '22px',
      marginRight: '12px',
    },
    paperAlertMessage: {
      padding: '8px 0',
      margin: 0,
    },
  })
)
