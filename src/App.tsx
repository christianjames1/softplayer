import Routes from './routes/Routes'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles'
import {useAppSelector} from './app/hooks'

import {selectCharacter} from './features/character/CharacterSlice'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
)

function App(): JSX.Element {
  const classes = useStyles()
  const character = useAppSelector(selectCharacter)
  const title = `Softplayer ${character.name ? ` - ${character.name}` : ''}`

  return (
    <div className="App">
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title} data-testid="title-page">
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      <Routes />
    </div>
  )
}

export default App
