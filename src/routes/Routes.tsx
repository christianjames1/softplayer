import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import {Character} from '../features/character/Character'
import {CharacterForm} from '../features/character/CharacterForm'
import {CharacterDetail} from '../features/character/CharacterDetail'

export default function App(): JSX.Element {
  return (
    <Router>
      <Switch>
        <Route path="/character/view/:id">
          <CharacterDetail />
        </Route>
        <Route path="/character/add">
          <CharacterForm />
        </Route>
        <Route path="/character/:id">
          <CharacterForm />
        </Route>
        <Route path="/">
          <Character />
        </Route>
      </Switch>
    </Router>
  )
}
