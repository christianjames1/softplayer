# Sofplayer

Para rodar o projeto a primeira vez é necessário instalar os pacotes utilizando o comando:

### `yarn install`ou `npm install`

após a instalação bastar rodar o comando `npm run start`

Comandos disponíveis para o projeto

### `npm run start` ou `yarn start`

Roda a aplicação em modo de desenvolvimento. <br />
Url: [http://localhost:3000](http://localhost:3000)

A pagina irá recarregar caso faça alguma alteração no código. <br />

### `npm run test` ou `yarn test`

Roda os testes. <br />

### `npm run build` ou `yarn build`

Cria um build da aplicação para produção na pasta `build`.<br />
